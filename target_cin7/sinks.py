"""cin7 target sink class, which handles writing streams."""

from singer_sdk.sinks import BatchSink
import requests
import base64


class cin7Sink(BatchSink):
    """cin7 target sink class."""

    max_size = 10
    base_url = "https://api.cin7.com/api/v1/"

    @property
    def authenticator(self):
        """Return authenticator header."""
        username=self.config.get("api_key")
        password=self.config.get("api_password")
        credentials = f"{username}:{password}".encode()
        auth_token = base64.b64encode(credentials).decode("ascii")
        auth_credentials = {"Authorization": f"Basic {auth_token}"}
        return auth_credentials

    def start_batch(self, context: dict) -> None:
        """Start a batch.
        
        Developers may optionally add additional markers to the `context` dict,
        which is unique to this batch.
        """
        if self.stream_name=="purchase_orders":
            context["url"] = f"{self.base_url}PurchaseOrders"

    def process_record(self, record: dict, context: dict) -> None:
        """Load the latest record from the stream."""
        if "records_put" not in context:
            context["records_put"] = []
            context["records_post"] = []
        record = {k: v for k, v in record.items() if v!=""}
        if record.get("id"):
            context["records_put"].append(record)
        else:
            context["records_post"].append(record)

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        if not context.get("url"):
            self.logger.warning(f"Stream {self.stream_name} not supported.")

        if len(context.get("records_put")):
            self.logger.info(f"Updating {self.stream_name} data")
            response = requests.put(context["url"], headers=self.authenticator, json=context["records_put"])
            for status in response.json():
                if status.get("success"):
                    self.logger.info(f"Reference {status.get('code')} updated succesfuly")
                elif status.get("success")==False:
                    self.logger.warning(f"It was not possible to update index {status.get('index')}: {status.get('errors')}")
        
        if len(context.get("records_post")):
            self.logger.info(f"Inserting new {self.stream_name} data")
            response = requests.post(context["url"], headers=self.authenticator, json=context["records_post"])
            for status in response.json():
                if status.get("success"):
                    self.logger.info(f"Reference {status.get('code')} inserted succesfuly")
                elif status.get("success")==False:
                    self.logger.warning(f"It was not possible to insert index {status.get('index')}: {status.get('errors')}")
