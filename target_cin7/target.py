"""cin7 target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_cin7.sinks import (
    cin7Sink,
)


class Targetcin7(Target):
    """Sample target for cin7."""

    name = "target-cin7"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True
        ),
        th.Property(
            "api_password",
            th.StringType,
            required=True
        ),
    ).to_dict()
    default_sink_class = cin7Sink


if __name__ == '__main__':
    Targetcin7.cli()    
